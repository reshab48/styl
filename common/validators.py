from django.template import loader
from django.core.exceptions import ValidationError

def validate_template(template_url):
    try:
        template = loader.get_template(template_url)
    except:
        raise ValidationError("Template not found")