from django import template
from shop.models import ProductColor
from account.models import WishList

register = template.Library()

def get_color_img(arg, slug):
    return ProductColor.objects.get(slug=slug).color_img

register.filter('get_color_img', get_color_img)

def get_color_hex(arg, slug):
    return ProductColor.objects.get(slug=slug).hex_value

register.filter('get_color_hex', get_color_hex)

def get_wishlist_count(arg, user):
    return len(WishList.objects.filter(user=user))

register.filter('get_wishlist_count', get_wishlist_count)