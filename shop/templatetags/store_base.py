from shop.models import StoreCategory, StoreSubCategory, ProductCategory
from django import template
from shop.forms import SearchForm

register = template.Library()

@register.inclusion_tag('menu.html')
def show_menu(base):
    store_categories = StoreCategory.objects.all()
    store_sub_categories = StoreSubCategory.objects.all()
    product_categories = ProductCategory.objects.all()
    return {'store_categories' : store_categories, 'store_sub_categories' : store_sub_categories, 'product_categories' : product_categories}

@register.inclusion_tag('search.html')
def show_search(base):
    search_form = SearchForm()
    return {'search_form' : search_form}

@register.inclusion_tag('search_xs.html')
def show_search_xs(base):
    search_form = SearchForm()
    return {'search_form' : search_form}