from shop.models import StoreSubCategory, StoreBanner, StoreCategoryBanner
from django import template

register = template.Library()

@register.inclusion_tag('banner.html')
def show_banner(landing):
    try:
        banner = StoreBanner.objects.get(active=True, location='SL')
    except Exception as e:
        banner = None
    store_sub_categories = StoreSubCategory.objects.all()
    return {'store_sub_categories' : store_sub_categories, 'banner' : banner}

@register.inclusion_tag('category_banners.html')
def show_category_banners(landing):
    try:
        banner_tl = StoreCategoryBanner.objects.get(banner_type='TL', active=True)
    except Exception as e:
        banner_tl = None
    try:
        banner_tr = StoreCategoryBanner.objects.get(banner_type='TR', active=True)
    except Exception as e:
        banner_tr = None
    try:
        banner_cl = StoreCategoryBanner.objects.get(banner_type='CL', active=True)
    except Exception as e:
        banner_cl = None
    try:
        banner_bl = StoreCategoryBanner.objects.get(banner_type='BL', active=True)
    except Exception as e:
        banner_bl = None
    try:
        banner_br = StoreCategoryBanner.objects.get(banner_type='BR', active=True)
    except Exception as e:
        banner_br = None
    return {'banner_tl' : banner_tl, 'banner_tr' : banner_tr, 'banner_cl' : banner_cl, 'banner_bl' : banner_bl, 'banner_br' : banner_br}