from django.shortcuts import render, redirect
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from shop.models import StoreCategory, StoreSubCategory, ProductCategory, Product, ProductColor, StoreBanner
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from shop.forms import ProductListFilterForm, SearchForm, ContactForm
from account.models import WishList
from common.decorators import ajax_required
from django.contrib import messages
# Create your views here.

@ensure_csrf_cookie
def index(request):
    return render(request, 'shop/index.html', {'section' : 'shop_index'})

def store_search(request):
    if not request.is_ajax():
        form = SearchForm(request.POST)
        if form.is_valid():
            query = form.cleaned_data['search']
        else:
            messages.error(request, 'Search query should not be empty')
            return redirect('shop:index')
    else:
        query = request.GET.get('query')
    items = Product.objects.filter(name__icontains=query, active=True)
    total = items.count()
    paginator = Paginator(items, 6)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        if request.is_ajax():
            return HttpResponse('')
        items = paginator.page(paginator.num_pages)
    try:
        search_banner = StoreBanner.objects.get(active=True, location='PS')
    except Exception as e:
        search_banner = None
    if request.is_ajax():
        return render(request, 'shop/items/search_results_ajax.html', {'section' : 'shop_search', 'items' : items})
    return render(request, 'shop/items/search_results.html', {'section' : 'shop_search', 'search_banner' : search_banner, 'items' : items, 'query' : query, 'count' : total})

def product_list(request, store_cat_slug=None, store_cat_id=None, store_sub_cat_slug=None, store_sub_cat_id=None, prod_cat_slug=None, prod_cat_id=None):
    store_category = None
    store_sub_category = None
    product_category = None
    colors = []
    sizes = []
    if store_cat_slug:
        store_category = StoreCategory.objects.get(id=store_cat_id, slug=store_cat_slug)
        items = Product.objects.filter(category__store_sub_category__store_category=store_category)
    if store_sub_cat_slug:
        store_sub_category = StoreSubCategory.objects.get(id=store_sub_cat_id, slug=store_sub_cat_slug)
        items = Product.objects.filter(category__store_sub_category=store_sub_category)
    if prod_cat_slug:
        product_category = ProductCategory.objects.get(id=prod_cat_id, slug=prod_cat_slug)
        items = Product.objects.filter(category=product_category)
    for item in items:
        for color in item.colors.all():
            if color not in colors:
                colors.append(color)
        for size in item.sizes.split(',')[:-1]:
            if size not in sizes:
                sizes.append(size)
    categories_tup = []
    sizes_tup = []
    colors_tup = []
    if store_category:
        for cat in store_category.store_sub_categories.all():
            choice = ('1/{}'.format(cat.id), cat.name)
            categories_tup.append(choice)
    elif store_sub_category:
        for cate in store_sub_category.categories.all():
            choice = ('2/{}'.format(cate.id), cate.name)
            categories_tup.append(choice)
    for siz in sizes:
        choice = (siz, siz)
        sizes_tup.append(choice)
    for colo in colors:
        choice = (colo.slug, colo.name)
        colors_tup.append(choice)
    if request.method == 'POST':
        filtered_items = []
        form = ProductListFilterForm(data=request.POST, categories=categories_tup, colors=colors_tup, sizes=sizes_tup)
        if form.is_valid():
            _filter = form.cleaned_data
            category_filter = _filter['categories'].split('/')
            try:
                if category_filter[0] == '1':
                    items = items.filter(category__store_sub_category__store_category=StoreCategory.objects.get(id=int(category_filter[1])))
                elif category_filter[1] == '1':
                    items = items.filter(category__store_sub_category=StoreSubCategory.objects.get(id=int(category_filter[1])))
                price_filter = _filter['price'].split('/')
            except IndexError as e:
                pass
            price_filter = _filter['price'].split('/')
            try:
                if int(price_filter[1]) == 0:
                    for price_f_item in items.filter(sale_price__gte=float(price_filter[0])):
                        fil
                else:
                    items = items.filter(sale_price__gte=float(price_filter[0]), sale_price__lte=float(price_filter[1]))
            except IndexError as e:
                pass
            colors_filter = _filter['colors']
            colors_f = []
            for color_f in colors_filter:
                colors_f.append(ProductColor.objects.get(slug=color_f))
            if len(colors_f) > 0:
                items = items.filter(colors__in=colors_f)
            size_filter = _filter['sizes']
            items = items.filter(sizes__icontains=size_filter)
    else:
        form = ProductListFilterForm(categories=categories_tup, colors=colors_tup, sizes=sizes_tup)
    items = items.distinct()
    total = items.count()
    paginator = Paginator(items, 6)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        if request.is_ajax():
            return HttpResponse('')
        items = paginator.page(paginator.num_pages)
    try:
        list_banner = StoreBanner.objects.get(active=True, location='PL')
    except Exception as e:
        list_banner = None
    if request.is_ajax():
        return render(request, 'shop/items/list_ajax.html', {'section' : 'shop_list', 'list_banner' : list_banner, 'form' : form, 'store_category' : store_category, 'store_sub_category' : store_sub_category, 'product_category' : product_category, 'items' : items})
    return render(request, 'shop/items/list.html', {'section' : 'shop_list', 'list_banner' : list_banner, 'form' : form, 'store_category' : store_category, 'store_sub_category' : store_sub_category, 'product_category' : product_category, 'items' : items})

def product_detail(request, product_slug):
    product = Product.objects.get(slug=product_slug)
    sizes = product.sizes.split(',')[:-1]
    images = product.images.all()
    try:
        detail_banner = StoreBanner.objects.get(active=True, location='PD')
    except Exception as e:
        detail_banner = None
    return render(request, 'shop/items/detail.html', {'section' : 'shop_detail', 'detail_banner' : detail_banner, 'product' : product, 'sizes' : sizes, 'images' : images})

@ajax_required
@require_POST
@login_required
def item_wished(request):
	item_id = request.POST.get('id')
	action = request.POST.get('action')
	if item_id and action:
		try:
			item = Product.objects.get(id=int(item_id))
			if action == 'wish':
				item.wished.add(request.user)
				WishList.objects.create(user=request.user, item=item)
			else:
				item.wished.remove(request.user)
				WishList.objects.get(user=request.user, item=item).delete()
			return JsonResponse({'status' : 'ok'})
		except:
			pass
	return JsonResponse({'status' : 'ko'})

def about_view(request):
    return render(request, 'shop/about.html', {'section' : 'shop_about'})

def contact_view(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Thank you for your message, we will contact you soon.")
            return redirect('shop:index')
    else:
        form = ContactForm()
    return render(request, 'shop/contact.html', {'section' : 'shop_contact', 'form' : form})

def policy_view(request):
    return render(request, 'shop/policy.html', {'section' : 'shop_policy'})

def terms_view(request):
    return render(request, 'shop/terms.html', {'section' : 'shop_terms'})