// Mobile Navigation
$('.nav__toggle-xs').click(function(){
    $('.header').toggleClass('open');
    $('.header__bt, main').click(function(){
        $('.header').removeClass('open');
    })
    return false;
});

// Mobile Search
$('.search-xs').click(function(){
    var search, type;
    search = $('.header__tp-xs-search');
    type = search.find('input');
    search.slideDown(function(){
        type.focus();
    });
    $('.header__tp-xs-search-close').click(function(){
        search.slideUp();
    })
    return false;
});

$(".megamenu > a").click(function(e) {
    e.preventDefault();
    var el = $(this);
    if(window.innerWidth > 600){
        $('.megamenu').removeClass('open');
    }
    el.parent().toggleClass('open');
    $('.header__bt, main').click(function(){
        $('.megamenu').removeClass('open');
    })
});

