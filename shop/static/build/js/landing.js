// Sliders
$('.hero__section-slide').owlCarousel({
    margin:10,
    nav: true,
    dots: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:4,
            nav: false,
            dots: true
        },
        600:{
            items:3,
            nav: false,
            dots: true
        },
        1000:{
            items:4
        }
    }
});