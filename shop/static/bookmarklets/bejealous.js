(window.myBookmarklet = function(){
    var jquery_version = '2.1.4';
    var site_url = 'https://styl.me.uk/';
    var static_url = site_url + 'static/';
    var min_width = 100;
    var min_height = 100;
    function bookmarklet(msg) {
        // load CSS
        var css = jQuery('<link>');
        css.attr({
            rel: 'stylesheet',
            type: 'text/css',
            href: static_url + 'bookmarklets/css/bejealous.css?r=' + Math.floor(Math.random()*99999999999999999999)
        });
        jQuery('head').append(css);
        // load HTML
        box_html = '<div id="mdl786Adevs"><div class="mdl786Adevs-content"><a class="mdl786Adevs-dismiss" href="Javascript:void(0)">&#x2716;</a><h3 style="text-align: center;">Bejealous Product Importer</h3><h4>Total Items : <span><b id="lsp34w-count">0</b></span><a class="btn btn-success mjk23-import-bulk" href="#" style="float: right; font-size: 12px;">Import All</a></h4><div class="item__importer-scroll"><div class="item__importer-container"></div></div></div></div>';
        jQuery('body').prepend(box_html);
        // close event
        jQuery('#mdl786Adevs .mdl786Adevs-dismiss').click(function(){
            jQuery('#mdl786Adevs').remove();
        });
        jQuery.each(jQuery('a[itemprop="url"]'), function(index, a_tag) {
            count = $('#lsp34w-count').text();
            $('#lsp34w-count').text(parseInt(count) + 1);
            product_url = 'https://bejealous.com' + jQuery(a_tag).attr('href');
            image_url = jQuery(a_tag).find('.image__container img').attr('src');
            product_name = jQuery(a_tag).find('.info .title').text();
            jQuery('#mdl786Adevs .item__importer-container').append('<div class="item__importer" data-url="'+product_url+'"><div class="item__importer-content"><div class="item__importer-img"><img src="'+image_url+'" alt=""></div><div class="item__importer-desc"><h3>'+product_name+'</h3><a href="#" class="btn btn-success msgc46-import">Import</a><a href="#" class="btn btn-danger lmps23-remove">Remove</a></div></div></div>');
        });
        $("#mdl786Adevs .lmps23-remove").click(function() {
            $(this).parent().parent().parent().remove();
            count = $('#lsp34w-count').text();
            $('#lsp34w-count').text(parseInt(count) - 1);
        });
        $("#mdl786Adevs .msgc46-import").click(function() {
            product_url = $(this).parent().parent().parent().attr('data-url');
            $.post('https://styl.me.uk/affiliate/advertiser-import',
            {
                'advertiser' : 'bejealous',
                'url' : product_url
            },
            function(data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
            });
            $(this).parent().parent().parent().addClass("imported");
            $(this).remove();
        });
        $("#mdl786Adevs .mjk23-import-bulk").click(function() {
            $("#mdl786Adevs .item__importer").each(function() {
                console.log($(this).attr('class').indexOf("imported"));
                if ($(this).attr('class').indexOf("imported") == -1) {
                    product_url = $(this).attr('data-url');
                    $.post('https://styl.me.uk//affiliate/advertiser-import',
                    {
                        'advertiser' : 'bejealous',
                        'url' : product_url
                    },
                    function(data, status) {
                        console.log("Data: " + data + "\nStatus: " + status);
                    });
                    $(this).addClass('imported');
                    $(this).find('.msgc46-import').remove();
                }
            });
        });
    };
    // Check if jQuery is loaded
    if(typeof window.jQuery != 'undefined') {
        bookmarklet();
    } else {
        // Check for conflicts
        var conflict = typeof window.$ != 'undefined';
        // Create the script and point to Google API
        var script = document.createElement('script');
        script.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/' + jquery_version + '/jquery.min.js');
        // Add the script to the 'head' for processing
        document.getElementsByTagName('head')[0].appendChild(script);
        // Create a way to wait until script loading
        var attempts = 15;
        (function(){
            // Check again if jQuery is undefined
            if(typeof window.jQuery == 'undefined') {
                if(--attempts > 0) {
                    // Calls himself in a few milliseconds
                    window.setTimeout(arguments.callee, 250)
                } else {
                    // Too much attempts to load, send error
                    alert('An error ocurred while loading jQuery')
                }
            } else {
                bookmarklet();
            }
        })(); }
    })()