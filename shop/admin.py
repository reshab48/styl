from django.contrib import admin
from shop.models import Product, ProductImage, ProductColor, StoreCategory, StoreSubCategory, ProductCategory, StoreCategoryBanner, StoreBanner, Contact
from django.utils.safestring import mark_safe
# Register your models here.

class ProductImageInline(admin.TabularInline):
    readonly_fields = ('image_preview',)

    model = ProductImage

    def image_preview(self, obj):
        if obj.image_url:
            return mark_safe('<img src="{}" width="180" height="300">'.format(obj.image_url))
        return "No Image Added"

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['thumb','advertiser', 'name', 'sale_price', 'market_price', 'active', 'sold_out']
    list_editable = ['sale_price', 'market_price', 'active']
    list_filter = ['category', 'active', 'created', 'updated']
    search_fields = ['name', 'category__name']
    date_hierarchy = 'created'
    inlines = [ProductImageInline]
    prepopulated_fields = {'slug' : ('name',)}
    list_per_page = 10

    def thumb(self, obj):
        return mark_safe('<img src="{}" width="90" height="150">'.format(obj.get_image()))

@admin.register(ProductColor)
class ProductColorAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug' : ('name',)}

@admin.register(StoreCategory)
class StoreCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'order']
    list_editable = ['order']
    prepopulated_fields = {'slug' : ('name',)}

@admin.register(StoreSubCategory)
class StoreSubCategoryAdmin(admin.ModelAdmin):
    list_display = ['store_category', 'name', 'preview', 'slug', 'order']
    list_editable = ['order']
    prepopulated_fields = {'slug' : ('name',)}
    readonly_fields = ['preview']

    def preview(self, obj):
        if obj.image:
            return mark_safe('<img src="{}" width="50px">'.format(obj.image.url))
        return mark_safe('<svg width="3rem" height="3rem"><use xlink:href="{}" /></svg>'.format(obj.icon))

@admin.register(ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ['store_sub_category', 'name', 'slug', 'order']
    list_editable = ['order']
    prepopulated_fields = {'slug' : ('name',)}

@admin.register(StoreCategoryBanner)
class StoreCategoryBannerAdmin(admin.ModelAdmin):
    list_display = ['store_category', 'title', 'preview', 'banner_type', 'active']
    list_filter = ['banner_type', 'active']
    readonly_fields = ['preview']

    def preview(self, obj):
        return mark_safe('<img src="{}" width="100" height="100">'.format(obj.image.url))

@admin.register(StoreBanner)
class StoreBannerAdmin(admin.ModelAdmin):
    list_display = ['preview', 'image', 'title', 'active']
    list_filter = ['active']
    list_editable = ['active']
    readonly_fields = ['preview']

    def preview(self, obj):
        return mark_safe('<img src="{}" width="400" height="200">'.format(obj.image.url))

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'email', 'phone']
