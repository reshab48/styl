from django.db import models
from affiliate.models import Advertiser
from django.urls import reverse
from django.core.validators import validate_comma_separated_integer_list, URLValidator
from django.core.exceptions import ValidationError
from affiliate.models import AdvertiserImport
from django.conf import settings

BANNER_TYPE = (
    ('TL', 'Top Left'),
    ('TR', 'Top Right'),
    ('CL', 'Center Large'),
    ('BL', 'Bottom Left'),
    ('BR', 'Bottom Right')
)

BANNER_LOCATION = (
    ('SL', 'Store Landing'),
    ('PL', 'Products List'),
    ('PS', 'Products Search'),
    ('PD', 'Product Detail')
)

class StoreCategory(models.Model):
    name = models.CharField(max_length=128)
    slug = models.SlugField()
    order = models.IntegerField(default=1)

    class Meta:
        verbose_name_plural = '1. Store categories'
        ordering = ('order',)

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('shop:store-category-product-list', args=[self.id, self.slug])

class StoreSubCategory(models.Model):
    store_category = models.ForeignKey(StoreCategory, related_name='store_sub_categories', on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    slug = models.SlugField()
    image = models.ImageField(upload_to='sub_category_icons/', blank=True)
    icon = models.CharField(max_length=512, blank=True)
    order = models.IntegerField(default=1)

    class Meta:
        verbose_name_plural = '2. Store sub categories'
        ordering = ('order',)

    def __str__(self):
        return "{} > {}".format(self.store_category.slug, self.slug)

    def get_absolute_url(self):
        return reverse('shop:store-sub-category-product-list', args=[self.id, self.slug])

class ProductCategoryQuerySet(models.QuerySet):
    def delete(self, *args, **kwargs):
        for obj in self:
            for prod in obj.products.all():
                if prod.active:
                    prod.category = None
                    prod.active = False
                    prod.save()
        super(ProductCategoryQuerySet, self).delete(*args, **kwargs)

class ProductCategory(models.Model):
    objects = ProductCategoryQuerySet.as_manager()
    store_sub_category = models.ForeignKey(StoreSubCategory, related_name='categories', on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    slug = models.SlugField()
    order = models.IntegerField(default=1)

    class Meta:
        verbose_name_plural = '3. Product categories'
        ordering = ('order',)

    def __str__(self):
        return "{} > {} > {}".format(self.store_sub_category.store_category.slug, self.store_sub_category.slug, self.slug)

    def get_absolute_url(self):
        return reverse('shop:product-category-product-list', args=[self.id, self.slug])

    def delete(self, *args, **kwargs):
        for prod in self.products.all():
            if prod.active:
                prod.category = None
                prod.active = False
                prod.save()
        super(ProductCategory, self).delete(*args, **kwargs)

class ProductColor(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField(unique=True)
    hex_value = models.CharField(max_length=7, blank=True)
    color_img = models.URLField(blank=True)

    class Meta:
        verbose_name_plural = '7. Product colors'

    def __str__(self):
        return self.name

class ProductQuerySet(models.QuerySet):

    def delete(self, *args, **kwargs):
        try:
            for obj in self:
                ad_import = AdvertiserImport.objects.get(product_url=obj.product_url)
                ad_import.live = True
                ad_import.save()
        except:
            pass
        super(ProductQuerySet, self).delete(*args, **kwargs)

class Product(models.Model):
    objects = ProductQuerySet.as_manager()
    advertiser = models.ForeignKey(Advertiser, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    slug = models.SlugField(unique=True)
    wished = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='items_liked')
    category = models.ForeignKey(ProductCategory, related_name='products', blank=True, null=True, on_delete=models.DO_NOTHING)
    affiliate_url = models.TextField(validators=[URLValidator()])
    product_url = models.TextField(validators=[URLValidator()])
    active = models.BooleanField(default=False)
    sold_out = models.BooleanField(default=False)
    sale_price = models.DecimalField(max_digits=22, decimal_places=2, blank=True, null=True)
    market_price = models.DecimalField(max_digits=22, decimal_places=2, blank=True, null=True)
    sizes = models.CharField(max_length=512, blank=True)
    colors = models.ManyToManyField(ProductColor, related_name='products', blank=True)
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = '6. Products'
        ordering = ('active', '-created', 'updated')

    def __str__(self):
        return self.name
    
    def clean(self, *args, **kwargs):
        if not self.category and self.active:
            raise ValidationError("Product cannot be active without a category.")
        super(Product, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Product, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        try:
            ad_import = AdvertiserImport.objects.get(product_url=self.product_url)
            ad_import.live = True
            ad_import.save()
        except:
            pass
        super(Product, self).delete(*args, **kwargs)

    def get_image(self):
        return self.images.all()[0].image_url

    def get_absolute_url(self):
        return reverse('shop:product-detail', args=[self.slug])

class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images', on_delete=models.CASCADE)
    image_url = models.URLField()

    def __str__(self):
        return self.product.name

class StoreCategoryBanner(models.Model):
    store_category = models.ForeignKey(StoreCategory, related_name='banners', on_delete=models.CASCADE)
    product_category = models.ForeignKey(ProductCategory, related_name='banners', on_delete=models.DO_NOTHING, blank=True, null=True)
    banner_type = models.CharField(max_length=2, choices=BANNER_TYPE)
    active = models.BooleanField(default=False)
    image = models.ImageField(upload_to='category_banners/')
    title = models.CharField(max_length=128)
    caption = models.CharField(max_length=128, blank=True)
    btn_text = models.CharField(max_length=128)

    class Meta:
        verbose_name_plural = '5. Store category banners'

    def __str__(self):
        return self.title

    def clean(self, *args, **kwargs):
        cat_banners = StoreCategoryBanner.objects.filter(active=True)
        if len(cat_banners) == 5 and self not in cat_banners and self.active:
            raise ValidationError("You already have 5 active category banners.")
        try:
            eqi_banner = cat_banners.get(banner_type=self.banner_type)
            if self.active and eqi_banner:
                raise ValidationError("You already have a category banner type: '{}' active.")
        except:
            pass
        super(StoreCategoryBanner, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(StoreCategoryBanner, self).save(*args, **kwargs)

    def get_absolute_url(self):
        if self.store_category and self.product_category:
            return self.product_category.get_absolute_url()
        if self.product_category:
            return self.product_category.get_absolute_url()
        return self.store_category.get_absolute_url()

class StoreBanner(models.Model):
    image = models.ImageField(upload_to='banners/')
    title = models.CharField(max_length=128, blank=True)
    location = models.CharField(max_length=2, choices=BANNER_LOCATION)
    active = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = '4. Store Banners'

    def __str__(self):
        return self.image.url

    def clean(self, *args, **kwargs):
        banners = StoreBanner.objects.filter(active=True)
        if len(banners) == 4 and self not in banners and self.active:
            raise ValidationError("You already have 4 active banners.")
        super(StoreBanner, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(StoreBanner, self).save(*args, **kwargs)

class Contact(models.Model):
    full_name = models.CharField(max_length=128)
    email = models.EmailField()
    phone = models.CharField(max_length=10, blank=True)
    message = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = '8. Contacts'

    def __str__(self):
        return self.full_name