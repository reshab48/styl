from django import forms
from shop.models import Contact

PRICE_CHOICES = [
    ('5/250', '£5 - £250'),
    ('250/500', '£250 - £500'),
    ('500/750', '£500 - ££750'),
    ('750/1000', '£750 - £1000'),
    ('1000/0', '£1000 +')
]

class ProductListFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes')
        self.colors = kwargs.pop('colors')
        self.categories = kwargs.pop('categories')
        super(ProductListFilterForm, self).__init__(*args, **kwargs)
        self.fields['categories'] = forms.ChoiceField(choices=self.categories, required=False)
        self.fields['price'] = forms.ChoiceField(choices=PRICE_CHOICES, required=False)
        self.fields['colors'] = forms.MultipleChoiceField(choices=self.colors, required=False)
        self.fields['sizes'] = forms.ChoiceField(choices=self.sizes, required=False)

class SearchForm(forms.Form):
    search = forms.CharField(max_length=512)

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = '__all__'