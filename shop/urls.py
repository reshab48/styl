from django.urls import path
from shop.views import index, store_search, product_list, product_detail, item_wished, about_view, contact_view, terms_view, policy_view

urlpatterns = [
    path('', index, name='index'),
    path('search', store_search, name='search'),
    path('products/category/<int:store_cat_id>/<str:store_cat_slug>', product_list, name='store-category-product-list'),
    path('products/sub-category/<int:store_sub_cat_id>/<str:store_sub_cat_slug>', product_list, name='store-sub-category-product-list'),
    path('products/product-category/<int:prod_cat_id>/<str:prod_cat_slug>', product_list, name='product-category-product-list'),
    path('product/<str:product_slug>', product_detail, name='product-detail'),
    path('product/wish/', item_wished, name='wish_product'),
    path('about-us/', about_view, name='about'),
    path('contact-us', contact_view, name='contact'),
    path('terms-and-conditions/', terms_view, name='terms'),
    path('privacy-policy/', policy_view, name='policy'),
]