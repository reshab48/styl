# Generated by Django 2.1.2 on 2019-01-08 15:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_contact'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contact',
            options={'verbose_name_plural': '8. Contacts'},
        ),
        migrations.AlterField(
            model_name='product',
            name='market_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=22),
        ),
        migrations.AlterField(
            model_name='product',
            name='sale_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=22),
        ),
    ]
