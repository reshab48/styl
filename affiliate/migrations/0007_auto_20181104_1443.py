# Generated by Django 2.1.2 on 2018-11-04 14:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('affiliate', '0006_auto_20181104_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advertiserimport',
            name='product_url',
            field=models.URLField(unique=True),
        ),
    ]
