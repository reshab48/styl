# Generated by Django 2.1.2 on 2018-11-04 11:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('affiliate', '0003_auto_20181104_1124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advertiser',
            name='bookmarklet_url',
            field=models.CharField(max_length=512),
        ),
    ]
