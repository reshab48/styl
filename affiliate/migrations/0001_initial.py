# Generated by Django 2.1.2 on 2018-10-31 17:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Advertiser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('logo', models.URLField()),
                ('deep_link', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='AdvertiserImport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_url', models.URLField()),
                ('live', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now=True)),
                ('updated', models.DateTimeField(auto_now_add=True)),
                ('advertiser', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='imports', to='affiliate.Advertiser')),
            ],
            options={
                'ordering': ('live', '-created'),
            },
        ),
    ]
