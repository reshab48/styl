from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_POST
from common.decorators import ajax_required
from django.views.decorators.csrf import csrf_exempt
from affiliate.models import Advertiser, AdvertiserImport
from django.core.exceptions import ValidationError
from django.http import JsonResponse, HttpResponseBadRequest
# Create your views here.

@csrf_exempt
@require_POST
def products_import(request):
    advertiser_slug = request.POST.get('advertiser')
    product_url = request.POST.get('url')
    if advertiser_slug and product_url:
        advertiser = get_object_or_404(Advertiser, slug=advertiser_slug)
        try:
            AdvertiserImport.objects.create(advertiser=advertiser, product_url=product_url)
        except:
            return HttpResponseBadRequest()
        return JsonResponse({'status' : 'ok'})
    return JsonResponse({'status' : 'ko'})
    