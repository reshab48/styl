from django.db import models
from common.validators import validate_template

class Advertiser(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField(unique=True)
    logo = models.URLField(blank=True)
    bookmarklet_url = models.CharField(max_length=512, validators=[validate_template], blank=True, null=True)
    deep_link = models.URLField()

    class Meta:
        verbose_name_plural = '1. Advertisers'

    def __str__(self):
        return self.name

class AdvertiserImport(models.Model):
    advertiser = models.ForeignKey(Advertiser, related_name='imports', on_delete=models.CASCADE)
    product_url = models.URLField(unique=True)
    live = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = '2. Advertiser imports'
        ordering = ('-live', '-created',)

    def __str__(self):
        return "{}: {}".format(self.advertiser, self.product_url)