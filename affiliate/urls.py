from django.urls import path
from affiliate.views import products_import

urlpatterns = [
    path('advertiser-import', products_import, name='advertiser-import'),
]