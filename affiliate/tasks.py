from bs4 import BeautifulSoup
from requests import get
from shop.models import Product, ProductColor, ProductImage
from affiliate.models import Advertiser
from celery.decorators import task
from django.utils.text import slugify
from random import randint
import urllib.parse
import re

@task(name='bejealous_product_create')
def bejealous_product_create(advertiser_prod_url):
    resp = get(advertiser_prod_url)
    content = resp.text
    soup = BeautifulSoup(content, 'html.parser')
    product_container = soup.find('div', {'id' : 'shopify-section-product-template'}).select("div.container.main.content.main-wrapper")[0]
    prod_name = product_container.find('h1', {'class' : 'product_name'}).text
    prod_price = product_container.find('span', {'itemprop' : 'price'})['content']
    try:
        prod_size_elems = product_container.find('div', {'data-option-index' : '0'}).select('div.swatch-element')
    except:
        prod_size_elems = []
    prod_sizes = ''
    for size in prod_size_elems:
        prod_sizes = prod_sizes + size['data-value'] + ','
    try:
        prod_color_elems = product_container.find('div', {'data-option-index' : '1'}).select('div.swatch-element')
    except:
        prod_color_elems = []
    prod_colors = []
    for color_elem in prod_color_elems:
        color_img = re.findall('https?://[^\)]+', color_elem.find('label')['style'])[0]
        color_name = color_elem['data-value']
        color_slug = slugify(color_name)
        try:
            color = ProductColor.objects.get_or_create(name=color_name, slug=color_slug, color_img=color_img)
            prod_colors.append(color[0])
        except:
            pass
    prod_desc = product_container.find('li', {'id' : 'description'}).find('div', {'class' : 'description-slide__inner'}).text
    product = None
    try:
        advertiser = Advertiser.objects.get(slug='bejealous')
        affiliate_url = advertiser.deep_link + urllib.parse.quote_plus(advertiser_prod_url)
        product = Product.objects.create(advertiser=advertiser, name=prod_name, slug=advertiser_prod_url.split('/')[-1], affiliate_url=affiliate_url, product_url=advertiser_prod_url, sale_price=int(float(prod_price)), market_price=int(float(prod_price))+randint(2, 5), sizes=prod_sizes, description=prod_desc)
        if product_container.find('span', {'class' : 'sold_out'}).text == 'Sold Out':
            product.sold_out = True
        for prod_color in prod_colors:
            product.colors.add(prod_color)
        product.save()
    except Exception as e:
        print(str(e))
        pass
    prod_img_elems = product_container.find('div', {'id' : 'product_slider'}).find('ul', {'class' : 'slides'}).find_all('li')
    if product:
        for img_elem in prod_img_elems:
            ProductImage.objects.create(product=product, image_url='https:'+img_elem['data-thumb'])
        return True
    return False