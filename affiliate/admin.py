from django.contrib import admin
from django.utils.html import format_html
from django.template import loader
from affiliate.models import Advertiser, AdvertiserImport
from affiliate.tasks import bejealous_product_create

@admin.register(Advertiser)
class AdvertiserAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'deep_link', 'advertiser_actions']
    prepopulated_fields = {'slug' : ('name',)}

    def advertiser_actions(self, obj):
        if obj.bookmarklet_url:
            return format_html('<a class="button" href="javascript:{}">{} Bookmarklet</a>', loader.get_template(obj.bookmarklet_url).render(), obj.name)
        return "No Bookmarklet"
    advertiser_actions.short_description = 'Advertiser Bookmarklets'
    advertiser_actions.allow_tags = True

def advertiser_product_create_action(modeladmin, request, queryset):
    for import_prod in queryset:
        if import_prod.live:
            bejealous_product_create.delay(import_prod.product_url)
            import_prod.live = False
            import_prod.save()
advertiser_product_create_action.short_description = 'Add Imports to Products'

@admin.register(AdvertiserImport)
class AdvertiserImportAdmin(admin.ModelAdmin):
    list_display = ['advertiser', 'product_url', 'live', 'created', 'updated']
    list_filter = ['advertiser__name', 'live', 'created', 'updated']
    date_hierarchy = 'created'
    actions = [advertiser_product_create_action,]
    list_per_page = 10