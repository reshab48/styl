from .base import *
import os

DEBUG = False
SECURE_SSL_REDIRECT = True
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_DOMAIN = "styl.me.uk"

ALLOWED_HOSTS = ['styl.me.uk', '127.0.0.1']

ADMINS = (
	('Reshab Das', 'das.reshab48@gmail.com'),
)

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'styl',
        'USER': 'postgres',
        'PASSWORD': 'styl@pass33',
        'HOST': 'localhost',
        'PORT': '',
    }
}

#celery settings
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Asia/Kolkata'

#email settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

LOGGING = {
    'version' : 1,
    'disable_existing_loggers' : False,
    'handlers' : {
        'disallowed' : {
            'level' : 'ERROR',
            'class' : 'logging.FileHandler',
            'filename' : os.path.join(BASE_DIR, 'disallowed_host.log'),
        },
    },
    'loggers' : {
        'django.security.DisallowedHost' : {
            'handlers' : ['disallowed'],
            'propagate' : False,
        },
    },
}