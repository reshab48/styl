from django.contrib.auth.models import User
from django import forms

class UserRegistrationForm(forms.ModelForm):
	email = forms.EmailField(max_length=100, required=True, widget=forms.TextInput(attrs={'placeholder' : 'someone@example.com'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Repeat Password', widget=forms.PasswordInput)
	
	
	class Meta:
		model = User
		fields = ('email',)

	def cleaned_password2(self):
		cd = self.cleaned_data
		if cd['password'] != cd['password2']:
			return forms.ValidationError('Your passwords didn\'t match')
		return cd['password2']