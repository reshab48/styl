from django.shortcuts import render, redirect
from django.contrib import messages
from account.forms import UserRegistrationForm
from django.contrib.auth.decorators import login_required
from account.models import WishList

# Create your views here.
def account_password_change_done(request):
	messages.success(request, "Account password successfully changed!")
	return redirect("account:login")

def account_password_reset_done(request):
	messages.success(request, "We have emailed you instructions for setting your password. If dont receive an e-mail, please make sure that you have entered the e-mail address you registered with.")
	return redirect("shop:index")

def account_password_reset_complete(request):
	messages.success(request, "Account password successfully reset!")
	return redirect("account:login")

def account_sign_up(request):
	if request.method == 'POST':
		form = UserRegistrationForm(request.POST)
		if form.is_valid():
			user = form.save(commit=False)
			user.username = user.email.split('@')[0]
			user.save()
			messages.success(request, 'Registration Successfull')
			return redirect('account:login')
	else:
		form = UserRegistrationForm()
	return render(request, 'registration/sign_up.html', {'form' : form})

@login_required
def account_wishlist(request):
	wish_items = WishList.objects.filter(user=request.user)
	count = len(wish_items)
	return render(request, 'wishlist.html', {'wish_items' : wish_items, 'count' : count})