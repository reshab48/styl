from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordResetView, PasswordResetConfirmView
from account.views import account_password_change_done, account_password_reset_complete, account_password_reset_done, account_sign_up, account_wishlist
from django.urls import reverse_lazy

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
	path('password_change/', PasswordChangeView.as_view(), {'post_change_redirect' : 'account:password_change_done'}, name='password_change'),
	path('password_change/done/', account_password_change_done, name='password_change_done'),
	path('password_reset/', PasswordResetView.as_view(success_url=reverse_lazy('account:password_reset_done')), name='password_reset'),
	path('password_reset/done/',account_password_reset_done, name='password_reset_done'),
	path('password_reset/confirm/<str:uidb64>/<str:token>/',PasswordResetConfirmView.as_view(success_url=reverse_lazy('account:password_reset_complete')), name='password_reset_confirm'),
	path('password_reset/complete/',account_password_reset_complete,name='password_reset_complete'),
	path('signup/', account_sign_up, name='signup'),
	path('wishlist/', account_wishlist, name='wishlist')
]