from django.db import models
from django.contrib.auth.models import User
from shop.models import Product

# Create your models here.
class WishList(models.Model):
	user = models.ForeignKey(User, db_index=True, on_delete=models.CASCADE)
	item = models.ForeignKey(Product, on_delete=models.CASCADE)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		unique_together = ('user', 'item')
		ordering = '-created', '-updated',

	def __unicode__(self):
		return "{}: {}".format(self.user, self.item.name)